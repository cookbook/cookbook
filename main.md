# Getting Started
Welcome to the Trisquel Developer Cookbook, an introductory guide written to help new users get started with Trisquel development.

## Table of Contents

# What Are Package Helpers?
`Package helpers` are [shell scripts](https://en.wikipedia.org/wiki/Shell_script) written in [GNU Bash](https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29) to fetch the source code of packages from Ubuntu repositories, [Launchpad](https://en.wikipedia.org/wiki/Launchpad_%28website%29), or other [APT](https://en.wikipedia.org/wiki/Advanced_Packaging_Tool) repositories and modify them as we want and compile them on Trisquel servers to be included in official Trisquel repositories.

# A Minimal Package Helper

```sh
#!/bin/sh
#
#    Copyright (C) 2012 Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=1
COMPONENT=main

. ./config
 
rm debian/update-notifier-hp-firmware.conf
sed '/hp-firmware/d' -i debian/update-notifier.install

changelog "Disabled hp-firmware handling"

compile
```
This package helper is named `make-update-notifier` and is available here: [https://devel.trisquel.info/trisquel/package-helpers/blob/belenos/helpers/make-update-notifier](https://devel.trisquel.info/trisquel/package-helpers/blob/belenos/helpers/make-update-notifier)

## Package Helper Name
Package helpers are named based on the following syntax,

```sh
make-sourcePackageName
```
Be aware that `SourcePackageName` is **NOT** necessarily the name of the package. For example, if you want to write a package helper for `gnome-colors-common` you can use the following command:

```sh
$ apt-cache show packageName
```
and for our example,

```sh
$ apt-cache show gnome-colors-common | head
Package: gnome-colors-common
Source: gnome-colors
Version: 5.5.1-1ubuntu1+7.0trisquel2
Architecture: all
Maintainer: Trisquel GNU/Linux developers <trisquel-devel@listas.trisquel.info>
Installed-Size: 8979
Depends: gnome-icon-theme-full (>= 2.24.0-4)
Homepage: http://code.google.com/p/gnome-colors/
Priority: optional
Section: gnome
```
and as you see in the second line of the output the sourcePackageName is `gnome-colors` and not gnome-colors-common. Therefore, you must name the package as

```sh
make-gnome-colors
```
The mentioned mistake [had occured](https://devel.trisquel.info/trisquel/package-helpers/commit/ad45954029eab66eb751b6e8720a46e142a8cad8) in Trisquel repositories but it was solved soon, 

# How to Contribute Your Code to the Trisquel Project on Gitlab

# creating an account on devel.trisquel.info

# Git

Git has been used as the revision control system for the development of Trisquel.

# installing git
git can be installed on your Trisquel system by using the command in the terminal:

	$ sudo apt install git git-doc
	
# git settings

	$ git config --global user.name 'Juddy Examlpe-Name'
	$ git config --global user.email 'example@example.org'
	
# creating an ssh key-pair

# uploading your ssh public key

# forking the package-helpers repository


# Cloning the forked repository

You must clone the appropriate repository based on the ...

	$ git clone git@devel.trisquel.info/MYUSERNAME/package-helpers

# changing the directory to the cloned repository

	$ cd package-helpers

# renaming the remote

	$ git remote rename origin myrepo
	
# adding the main repostitory as a remote

	$ git remote add upstream git@devel.trisquel.info/trisquel/package-helpers
	
ATTENTION: you are advised to create to a new branch before changing the files. Assume we want to fix a bug in the package called `zip`. 

	$ git branch fix-zip #creating a new branch
	$ git checkout fix-zip #changing the working directory to the new branch
	

# different types of packages

## Importing from upstream deb repostiory;
## removing non-free files


# License
Copyright (c) 2014 2016 Salman Mohammadi <salman@ubuntu.ir>

The contents of this page are licensed under the [GNU Free Documentation License](https://gnu.org/licenses/fdl.html) (v. 1.3 or any later version) with no invariant sections. The codes are dual licensed under [GNU General Public License](https://gnu.org/licenses/gpl.html) (v. 2 or any later version) or [GNU Free Documentation License](https://gnu.org/licenses/fdl.html) (v. 1.3 or any later version).
